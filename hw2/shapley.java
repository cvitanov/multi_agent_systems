// Java implementation of Shapley Value Calculations
// HW2 CIS 410
// By Andrew Cvitanovich
// Tested with JDK 8u212

import java.io.*;
import java.util.*;

public class shapley {
	//ArrayList for storing coalitions
	static ArrayList<ArrayList<Integer> > C = 
			new ArrayList<ArrayList<Integer> >();
	
	//ArrayList for storing coalition valuations
	static ArrayList<Integer> V = new ArrayList<Integer> ();
	
	//Number of players
	static int N;
	
    public static void main(String[] args) throws FileNotFoundException {
        //Get scanner instance
        Scanner scanner = new Scanner(new File("gameShapley.txt"));
         
        //Set the delimiter used in file
        scanner.useDelimiter("\n");
		
        //Number of Players in Game
        shapley.N = Integer.parseInt(scanner.next());
        //System.out.print("N = " + shapley.N + "\n");
        

        // Scan each line for coalition data!
        String str;
        while (scanner.hasNext())
        {
			str = scanner.next();
			boolean isCoalitionScanned = false;
			ArrayList<Integer> temp = 
				new ArrayList<Integer>();
			String numstr = "";
			
			// parse data for a coalition
			for ( int i = 0, n = str.length(); i < n; i++ ) {
				char c = str.charAt(i);
				if ( c == '{' ) {
					numstr = "";
				}
				if ( !(isCoalitionScanned) && Character.isDigit(c) ) {
					numstr += c;
				}
				if ( !(isCoalitionScanned) && c == ',' ) {
					temp.add(Integer.parseInt(numstr));
					numstr = "";
				}
				if ( c == '}' ) {
					isCoalitionScanned = true;
					temp.add(Integer.parseInt(numstr));
					numstr = "";
				}
				if ( isCoalitionScanned && c == ',' ) {
					numstr = "";
				}
				if ( isCoalitionScanned && Character.isDigit(c) ) {
					numstr += c;
				}
			}
			
			// add coalition data to arrays
			shapley.C.add(temp);
			shapley.V.add(Integer.parseInt(numstr));
			
		}
		
		// check if empty coalition exists, if not create a dummy empty coalition with payoff of zero
		boolean emptySetExists = false;
		for ( int i = 0; i < shapley.C.size(); i++ ) {
			if (shapley.C.get(i).size() == 0) {
				emptySetExists = true;
			}
		}
		if(!(emptySetExists)) {
				ArrayList<Integer> temp = new ArrayList<Integer> ();
				temp.clear();
				shapley.C.add(temp);
				shapley.V.add(0);
		}
		
		// write output to file Shapley.txt
		FileOutputStream fos = null;
		File file;
		String output = "";
		
		// loop through all players and calculate Shapley Values
		for ( int p = 1; p <= shapley.N; p++) {
			double payoff = shapleyValue(p);
			output += Integer.toString(p) + "," + Double.toString(payoff) + "\n";
			System.out.println("Shapley Value for Player " + p + " is " + payoff);
		}
		
		try {
				file = new File("Shapley.txt");
				fos = new FileOutputStream(file);
				if(!file.exists()) {
					file.createNewFile();
				}
				byte[] bytesArray = output.getBytes();
				fos.write(bytesArray);
				fos.flush();
				System.out.println("Shapley Values written to Shapley.txt");
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			try {
				if (fos != null) {
					fos.close();
				}
			}
			catch (IOException ioe) {
				System.out.println("Error closing file output stream!");
			}
		}
		for ( int i = 0; i < shapley.C.size(); i++ ) {
			;
			//System.out.println(shapley.C.get(i));
		}
         
        //Do not forget to close the scanner 
        scanner.close();
    }
    
    public static long fact(int n) {
		if (n == 0) {
			return 1;
		}
		if (n <= 2) {
			return n;
		}
		return n * fact(n-1);
	}
	
	// test if two coalitions are the same
	public static boolean compareCoalitions(ArrayList<Integer> a, ArrayList<Integer> b) {
		Collections.sort(a);
		Collections.sort(b);
		return a.equals(b);
	}
	
	// does coalition contain player p
	public static boolean hasPlayer(int p, ArrayList<Integer> c) {
		for (int i = 0, n = c.size(); i < n; i++) {
				if( p == c.get(i) ) {
					return true;
				}
		}
		return false;
	}
	
	// find index of a coalition in data
	public static int indexCoalition(ArrayList<Integer> c) {
		int index = -1;
		Iterator<ArrayList<Integer> > i = shapley.C.iterator();
		while (i.hasNext()) {
			index++;
			ArrayList<Integer> temp = i.next();
			if(compareCoalitions(temp, c)) {
				return index;
			}
		}
		return -1;
	}
	
	// Shapley helper function (calculate term of the sum for a coalition)
	// for player i
	public static long shapleyHelper(int i, ArrayList<Integer> c) {
		
		// get valuation for coalition c
		int idx_c = indexCoalition(c);
		//System.out.println("DEBUG: c is " + c);
		int v_c = shapley.V.get(idx_c);
		//System.out.println("DEBUG: v_c is " + v_c);
		
		// get valuation for coalition c union {i}
		ArrayList<Integer> c_union_i = new ArrayList<Integer> ();
		for(int j = 0, n = c.size(); j < n; j++) {
			c_union_i.add(c.get(j));
		}
		c_union_i.add(i);
		int idx_c_union_i = indexCoalition(c_union_i);
		//System.out.println("DEBUG: c_union_i is " + c_union_i);
		//System.out.println("DEBUG: idx_c_union_i = " + idx_c_union_i);
		int v_c_union_i = shapley.V.get(idx_c_union_i);
		//System.out.println("DEBUG: v_c_union_i = " + v_c_union_i);

		// return calculation
		//System.out.println("DEBUG: fact(c.size()) = " + fact(c.size()));
		//System.out.println("DEBUG: fact(shapley.N - c.size() - 1) = " + fact(shapley.N - c.size() - 1));
		long result = fact(c.size()) * fact(shapley.N - c.size() - 1) * ( v_c_union_i - v_c );
		//System.out.println("DEBUG: Result = " + result);
		return result;
		
	}
	
	// calculate Shapley value for player i
	// adds the calculations from shapleyHelper to compute the sum
	public static double shapleyValue(int i) {
		long result = 0;
		//System.out.println("DEBUG: Shapley for player " + i);
		Iterator<ArrayList<Integer> > itr = shapley.C.iterator();
		while ( itr.hasNext() ) {
			ArrayList<Integer> coalition = itr.next();
			if ( !(hasPlayer(i,coalition)) ) {
				long temp = shapleyHelper(i, coalition);
				//System.out.println("DEBUG: subresult = " + temp);
				result += temp;
			}
		}
		//System.out.println("DEBUG: sum of results = " + result);
		long nfact = fact(shapley.N);
		//System.out.println("DEBUG: nfact = " + nfact);
		double avg = 1.0 / ((double) nfact);
		//System.out.println("DEBUG: avg = " + avg);
		return avg * (double) result;
	}
	
}
