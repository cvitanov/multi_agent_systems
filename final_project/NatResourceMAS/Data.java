package NatResourceMAS;

import java.io.*;
import java.util.*;
import java.lang.*;

public class Data
{
 
 // Simulation Dimensions
 
 public static int Width = 1024;
 public static int Height = 800;
 public static int GridSize = 30;
 public static int TimeStep = 1;
 public static int nsteps = 50;
 public static int rows;
 public static int cols;
 public static int ncells;
 public static int population = 0;
 public static int max_possible_population = 0;
 public static int nhouses;
 public static double fox_prevalence = 0.1;
 
 // Data Arrays
 
 static ArrayList<ArrayList<String> > LandUse = 
  new ArrayList<ArrayList<String> >();
 
 static ArrayList<ArrayList<Boolean> > HasMinerals =
  new ArrayList<ArrayList<Boolean> >();
   
 static ArrayList<ArrayList<Boolean> > HasFoxes =
  new ArrayList<ArrayList<Boolean> >();
 
 static ArrayList<ArrayList<Integer> > nPeople =
  new ArrayList<ArrayList<Integer> >();
 
    // static variable single_instance of type Data 
    private static Data data = new Data(); 
  
    // private constructor restricted to this class itself 
    private Data() 
    { 
        // initialize simulation data
        rows = (Height / GridSize) - 2;
        cols = (Width / GridSize) - 2;
        ncells = rows * cols;
        //System.out.println("Cols = " + cols);
        
        // randomly distribute minerals and foxes
        Random random = new Random();
        
        for(int i = 0; i < rows; i++) {
          ArrayList<String> temp = new ArrayList<String> ();
          ArrayList<Boolean> temp_minerals = new ArrayList<Boolean> ();
          ArrayList<Boolean> temp_foxes = new ArrayList<Boolean> ();
          ArrayList<Integer> temp_people = new ArrayList<Integer> ();
          for(int j = 0; j < cols; j++) {
            temp.add("FOREST");
            
            // randomly distribute minerals
            double rand = random.nextDouble();
            if(rand < 0.333) {
              temp_minerals.add(true);
            } else {
              temp_minerals.add(false);
            }
            
            // randomly distribute foxes
            rand = random.nextDouble();
            if(rand < fox_prevalence) {
              temp_foxes.add(true);
            } else {
              temp_foxes.add(false);
            }
            
          }
          LandUse.add(temp);
          HasMinerals.add(temp_minerals);
          HasFoxes.add(temp_foxes);
          nPeople.add(temp_people);
          //System.out.println("Size dim1 : " + LandUse.size());
          //System.out.println("Size dim2 : " + LandUse.get(0).size());
        }
  

        
    } 
  
    // static method to create instance of Data class 
    public static Data getInstance() 
    { 
        return data; 
    }
    
    protected static void whoAmI() {
  System.out.println("Singleton Data Object!");
 }
 
 public static boolean hasMinerals(int row, int col) {
   return HasMinerals.get(row).get(col);
 }
 
 public static boolean hasFoxes(int row, int col) {
   return HasFoxes.get(row).get(col);
 }
 
 public static boolean hasForest(int row, int col) {
   if(LandUse.get(row).get(col) == "FOREST") {
    return true;
   } else {
    return false;
   }
 }
 
 public static boolean canFarm(int row, int col) {
  if(LandUse.get(row).get(col) == "LOGGING" || LandUse.get(row).get(col) == "FARM") {
   return true;
  } else {
   return false;
  }
 }
 
 public static boolean developmentCost(int row, int col, String new_type) {
   if(LandUse.get(row).get(col) == new_type) {
     return false;
   } else {
     return true;
   }
 }
 
 // return a value between 0 and 1 to indicate housing density nearby
 public static double localHousingFactor(int row, int col, int spread) {
   double factor = 0;
   for(int i = 1; i < spread; i++) {
	if( (row - i) >= 0 && LandUse.get(row-i).get(col) == "HOUSING") {factor += 0.25;}
	if( (row + i) < rows && LandUse.get(row+i).get(col) == "HOUSING") {factor += 0.25;}
	if( (col - i) >= 0 && LandUse.get(row).get(col-i) == "HOUSING") {factor += 0.25;}
	if( (col + i) < cols && LandUse.get(row).get(col+i) == "HOUSING") {factor += 0.25;}
   }
   return factor / spread;
 }
 
  // return a value between 0 and 1 to indicate intact forest nearby
 public static double localForestFactor(int row, int col, int spread) {
   double factor = 0;
   for(int i = 1; i < spread; i++) {
	    if( (row - i) >= 0 && LandUse.get(row-i).get(col) == "FOREST") {factor += 0.25;}
		if( (row + i) < rows && LandUse.get(row+i).get(col) == "FOREST") {factor += 0.25;}
		if( (col - i) >= 0 && LandUse.get(row).get(col-i) == "FOREST") {factor += 0.25;}
		if( (col + i) < cols && LandUse.get(row).get(col+i) == "FOREST") {factor += 0.25;}
	   
   }
   return factor / spread;
 }
 
   // return a value between 0 and 1 to indicate foxes nearby
 public static double localFoxesFactor(int row, int col, int spread) {
   double factor = 0;
   for(int i = 1; i < spread; i++) {
	    if( (row - i) >= 0 && HasFoxes.get(row-i).get(col)) {factor += 0.25;}
		if( (row + i) < rows && HasFoxes.get(row+i).get(col)) {factor += 0.25;}
		if( (col - i) >= 0 && HasFoxes.get(row).get(col-i)) {factor += 0.25;}
		if( (col + i) < cols && HasFoxes.get(row).get(col+i)) {factor += 0.25;}
	   
   }

   return factor / spread;
 }
 
 public static void setLandUse(int row, int col, String type) {
  LandUse.get(row).set(col, type);
 }
 
 // FIX: try to migrate fox???
 public static void removeFox(int row, int col) {
  HasFoxes.get(row).set(col,false);
 }
 
 public static void printLandUse() {
     // print
   //System.out.println("LandUse Data: " + LandUse.size());
      for(int i = 0; i < LandUse.size(); i++){
    //System.out.println("Col " + i + "Size = " + LandUse.get(i).size() + " : ");
          for(int j = 0; j < LandUse.get(i).size(); j++){
              //System.out.print("Row " + j + " " + LandUse.get(i).get(j) + ",");
          }
          //System.out.println();
      }
    }
    
//    public static void updateHabitatFactor() {
//   int forest_count = 0;
//   for(int i = 0; i < LandUse.size(); i++){
//          for(int j = 0; j < LandUse.get(i).size(); j++){
//     if(LandUse.get(i).get(j) == "FOREST") {
//      forest_count++;
//     }
//          }
//      }
//      habitatFactor = 1 - (forest_count / ncells);
//      
// }
 
    public void updatePopulation(double pop_density) {
      nhouses = 0;
      for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
          if(LandUse.get(i).get(j) == "HOUSING") {
            nhouses++;
          }
        }
      }
      population = nhouses * (int) pop_density;
      max_possible_population = rows * cols * (int) pop_density;
    }

} 
