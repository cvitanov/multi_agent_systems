import NatResourceMAS.*;
import java.util.*;
import java.lang.*;

public class run_NatResourceMAS
{
 // PARAMETERS:
  
 // housing density (people per housing development)
 public static int pop_density = 5;
 
 public static Data myData;
 public static int nPlayers;
 public static ArrayList<Player> plist = new ArrayList<Player> ();
 public static int current_winner;
 public static double current_seller_revenue;
 public static double auction_likelihood_factor = 0.1; // Only auction a third of cells each time step
 
 public static void main(String args[])
 {
  
  myData = Data.getInstance();
  DrawGrid myGrid = new DrawGrid("Land Use Model");
  
  // create players
  plist = new ArrayList<Player> ();
  
  plist.add(new Player("farmer","FARM",0));
  plist.add(new Player("housing","HOUSING",1));
  plist.add(new Player("miner","MINING",2));
  plist.add(new Player("logging","LOGGING",3));
  plist.add(new Player("nature conservancy","FOREST",4));
  
  nPlayers = plist.size();
  
  for(int k = 0; k < nPlayers; k++) {
   //System.out.println("Player/LandUse " + plist.get(k).type + " " + plist.get(k).land_use);
  }
  
  Random randval = new Random();

  // main loop
  for (int i = 0; i < myData.nsteps; i++) {
   
   try {
    Thread.sleep(100);
   } catch (InterruptedException e) {
    e.printStackTrace();
   }
   
   //myData.printLandUse();
   
   // TODO: update cell data (forest regrowth, species health, etc.)
   
   // UPDATE DATA:
   myData.updatePopulation(pop_density);
   
   // TODO: auction each cell off to highest bidder
   // (farmer, housing, miner, logger, nature conservancy)
   // players start out with X funds
   // original seller??? gov't???
   // need player objects with valuation calculations
   // auction method determines payoffs for winning bidders
   // player objects accumulate payoffs each time they win
   // a bid
   
   //System.out.println("num Rows/Cols = " + myData.rows + " " + myData.cols);
   for(int c = 0; c < myData.cols; c++) {
    for (int r = 0; r < myData.rows; r++) {
      if(randval.nextDouble() < auction_likelihood_factor) {
        //System.out.println("Auction row/col" + r + " " + c);
        decideWinnerOptimalAuction(r,c); // perform auction for row/col
      }
    }
   }
   
   // TODO: update environment data (habitat fragmentation, etc.)
   
   //myGrid.removeAll();
   //myGrid.revalidate();
   // draw grid data again
   myGrid.repaint();
   
   myData.TimeStep = i + 1;

     

  }

  System.out.println("Cumulative Payoffs for Each Player: ");
  for (Player p: plist) {
    System.out.println("Type: " + p.type + " Payoff Total: " + p.cumulative_payoffs);
  }

 }
 

 
 // decide winner of optimal auction
 // returns ID of winning player
 // determine player with highest virtual valuation
 // return player's ID
 public static void decideWinnerOptimalAuction(int row, int col) {
   ArrayList<Double> vvals = new ArrayList<Double> ();
   // Find the player with the maximal virtual valuation
   // (argmax{i} of phi(v_i))
   for(int i = 0; i < nPlayers; i++) {
     vvals.add( plist.get(i).virtual_valuation(row,col) );
   }
   double max_val = Math.round( (double)Collections.max(vvals) ); // maximum valuation
   // set of players matching max valuation
   // randomly choose one if the set exceeds 1 player
   ArrayList<Integer> max_set = new ArrayList<Integer> ();
   int count = 0;
   double second_highest = 0;
   double inf_v_star = 0;
   // find set of maximally valued players
   // also find second highest bid (To determine what payoff and seller revenue)
   int idx = 0;
   for (Player p: plist) {
     double vv = Math.round( (double) p.virtual_valuation(row,col) );
     double v = Math.round( (double) p.valuation(row,col));
     //System.out.println("Player #" + idx + " vv = " + vv + " v = " + v);
     if( max_val == vv ) {
       max_set.add(p.ID);
       count++;
     }
     if( max_val > vv && vv > second_highest ) {
       second_highest = vv;
       inf_v_star = v;
     }
     idx++;
   }
   Random rand = new Random();
   int id_winner = max_set.get(rand.nextInt(max_set.size()));
   //System.out.println("Winner is " + id_winner);
   
   // update current winner
   current_winner = id_winner;
   
   // calculate seller revenue
   current_seller_revenue = inf_v_star;
   
   // calculate payoff to winner and allocate to accumulated payoffs
   double pay = plist.get(current_winner).valuation(row,col) - current_seller_revenue;
   plist.get(current_winner).payoff(pay); // update cumulative payoffs for winner of optimal auction
   //System.out.println("Payoff is " + pay);
   
   

     // update data on land use for row/col
  String land_use = plist.get(id_winner).land_use;
  plist.get(id_winner).cells_utilized++;
  //System.out.println("Land Use for row/col : " + row + " " + col + " is " + land_use);
  myData.setLandUse(row,col,land_use);
  
       // if no longer habitat for foxes they disappear???
  if( myData.hasFoxes(row,col) && !(myData.hasForest(row,col)) ) {
    myData.removeFox(row,col);
  }
  
 }
 
  // auction to determine winner of a sealed bid second price auction and assign payoffs
 public static void sealedBidSecondPriceAuction(int row, int col) {
  
  // find max valuation
  ArrayList<Double> vals = new ArrayList<Double> ();
  for (int i = 0; i < nPlayers; i++) {
   vals.add( plist.get(i).valuation(row,col) );
  }
  double max_val = Math.round( (double)Collections.max(vals) ); // maximum valuation
  
  // set of players matching max valuation
  // randomly choose one if the set exceeds 1 player
  ArrayList<Integer> max_set = new ArrayList<Integer> ();
  int count = 0;
  double second_highest = 0;
  // find set of maximally valued players
  // also find second highest bid
  for (Player p: plist) {
    double v = Math.round( (double) p.valuation(row,col) ); 
    if( max_val == v ) {
     max_set.add(p.ID);
     count++;
    }
    if( max_val > v && v > second_highest ) {
     second_highest = v;
    }
  }
  Random rand = new Random();
  int id_winner = max_set.get(rand.nextInt(max_set.size()));
  //System.out.println("Winner is " + id_winner);
  // update data on land use for row/col
  String land_use = plist.get(id_winner).land_use;
  plist.get(id_winner).cells_utilized++;
  //System.out.println("Land Use for row/col : " + row + " " + col + " is " + land_use);
  myData.setLandUse(row,col,land_use);
  
  // if no longer habitat for foxes they disappear???
  if( myData.hasFoxes(row,col) && !(myData.hasForest(row,col)) ) {
    myData.removeFox(row,col);
  }
  
  // payoff goes to winner
  double thePayoff = max_val - second_highest;
  plist.get(id_winner).payoff(thePayoff);
  
 }
}



