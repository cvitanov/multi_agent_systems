import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.lang.*; // floor function
 
/**
 *
 * @web http://java-buddy.blogspot.com/
 */
public class JavaTestSwing {
     
    static JFrameWin jFrameWindow;
     
    public static class MyComponent extends JComponent{
 
        @Override
        protected void paintComponent(Graphics g) {
            try {
                //prepare a original Image source
                Image image = ImageIO.read(this.getClass().getResource("duke.png"));
 
                int w = image.getWidth(null);
                int h = image.getHeight(null);
                double scale = 0.25;
                g.drawImage(image, 0, 0, (int)Math.floor(w*scale), (int)Math.floor(h*scale), null);
                double old_scale = scale;
                scale = 0.5;
                g.drawImage(image, (int)Math.floor(w*old_scale), 0, (int)Math.floor(w*scale), (int)Math.floor(h*scale), null);
                 
            } catch (IOException ex) {
                Logger.getLogger(JavaTestSwing.class.getName()).log(Level.SEVERE, null, ex);
            }
 
        }
    }
     
    public static class JFrameWin extends JFrame{
        public JFrameWin(){
            this.setTitle("java-buddy.blogspot.com");
            this.setSize(300, 300);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  
            MyComponent myComponent = new MyComponent();
            this.add(myComponent);
        }
    }
 
    public static void main(String[] args){
        Runnable doSwingLater = new Runnable(){
             
            @Override
            public void run() {
                jFrameWindow = new JFrameWin();
                jFrameWindow.setVisible(true);
            }
        };
         
        SwingUtilities.invokeLater(doSwingLater);
         
    }
 
}
