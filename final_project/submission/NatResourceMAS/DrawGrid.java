// draw a simple grid for displaying the results of the simulation!

package NatResourceMAS;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
import javax.imageio.*;
import javax.swing.*;
import java.util.Random;
import java.awt.geom.Rectangle2D;
import java.lang.*; // floor function



public class DrawGrid extends Frame {

  Data myData;
  private int Height;
  private int Width;
  private int GridSize;
  private BufferedImage img = null;
  private BufferedImage minerals_img = null;
  private BufferedImage fox_img = null;
  private BufferedImage forest_img = null;
  private BufferedImage house_img = null;
  private BufferedImage mine_img = null;
  private BufferedImage logged_img = null;
  private Image transparent_img = null;
  private Image transparent_minerals_img = null;
  private Image transparent_fox_img = null;
  private Image transparent_forest_img = null;
  private Image transparent_house_img = null;
  private Image transparent_mine_img = null;
  private Image transparent_logged_img = null;

  public DrawGrid ( String title ) {
    super( title );
	myData = Data.getInstance();
	Height = myData.Height;
	Width = myData.Width;
	GridSize = myData.GridSize;
    setSize( Width, Height );
    addWindowListener( new WindowAdapter() {
      public void windowClosing( WindowEvent we ) {
        dispose();
        System.exit( 0 );
      }
    } );
    setVisible( true );
    
        // read image files
    try {
		int color;
		File file = new File("./images/farm2.jpg");
		img = ImageIO.read(file);
		color = img.getRGB(0, 0);
		transparent_img = makeColorTransparent(img, new Color(color));
		File file2 = new File("./images/minerals.jpg");
		minerals_img = ImageIO.read(file2);
		color = minerals_img.getRGB(0, 0);
		transparent_minerals_img = makeColorTransparent(minerals_img, new Color(color));
		File file3 = new File("./images/fox2.jpg");
		fox_img = ImageIO.read(file3);
		color = fox_img.getRGB(0, 0);
		transparent_fox_img = makeColorTransparent(fox_img, new Color(color));
		File file4 = new File("./images/forest.jpg");
		forest_img = ImageIO.read(file4);
		color = forest_img.getRGB(0, 0);
		transparent_forest_img = makeColorTransparent(forest_img, new Color(color));
		File file5 = new File("./images/house.jpg");
		house_img = ImageIO.read(file5);
		color = house_img.getRGB(0, 0);
		transparent_house_img = makeColorTransparent(house_img, new Color(color));
		File file6 = new File("./images/mine.jpg");
		mine_img = ImageIO.read(file6);
		color = mine_img.getRGB(0, 0);
		transparent_mine_img = makeColorTransparent(mine_img, new Color(color));
		File file7 = new File("./images/logging.jpg");
		logged_img = ImageIO.read(file7);
		color = logged_img.getRGB(0, 0);
		transparent_logged_img = makeColorTransparent(logged_img, new Color(color));
	} catch (IOException e) {
		System.out.println("Image Reading Failed! " + e);
	}
  }

  public void paint( Graphics g ) {
	
    Graphics2D g2d = (Graphics2D) g;

    

    
    // draw objects on the grid
    Random random = new Random();
    for ( int i = 1; i < ((Width / GridSize) - 1); i++) {
		for ( int j = 1; j < ((Height / GridSize) - 1); j++) {
			//System.out.println("Painting row/col : " + j + " " + i);
			//try {
				//Thread.sleep(200);
			//} catch (InterruptedException e) {
				//e.printStackTrace();
			//}
        	double rand = random.nextDouble();
        	String LandUseType = myData.LandUse.get(j - 1).get(i - 1);
        	
			if(LandUseType == "FARM") {

				g2d.drawImage(transparent_img, i * GridSize,
					j * GridSize, (int)Math.floor(GridSize), (int)Math.floor(GridSize), null);
			} else if (LandUseType == "LOGGING") {
				g2d.drawImage(transparent_logged_img, i * GridSize,
					j * GridSize, (int)Math.floor(GridSize), (int)Math.floor(GridSize), null);
			} else if (LandUseType == "HOUSING") {
				g2d.drawImage(transparent_house_img, i * GridSize,
					j * GridSize, (int)Math.floor(GridSize), (int)Math.floor(GridSize), null);
			} else if (LandUseType == "MINING") {
				g2d.drawImage(transparent_mine_img, i * GridSize,
					j * GridSize, (int)Math.floor(GridSize), (int)Math.floor(GridSize), null);
			} else {
				// forest habitat
				g2d.drawImage(transparent_forest_img, i * GridSize,
					j * GridSize, (int)Math.floor(GridSize), (int)Math.floor(GridSize), null);
			}

			boolean MineralsPresent = myData.HasMinerals.get(j - 1).get(i - 1);
			if( MineralsPresent ) {
				g2d.drawImage(transparent_minerals_img, i * GridSize, j * GridSize, (int)Math.floor(0.5 * GridSize), (int)Math.floor(0.5 * GridSize), null);
			}
			boolean FoxesPresent = myData.HasFoxes.get(j - 1).get(i - 1);
			if( FoxesPresent ) {
				g2d.drawImage(transparent_fox_img, i * GridSize + GridSize - (int)Math.floor(0.5 * GridSize),
					j * GridSize + GridSize - (int)Math.floor(0.5 * GridSize), 
					(int)Math.floor(0.5 * GridSize), (int)Math.floor(0.5 * GridSize), null);
			}

		}
    }
    
	g2d.setColor( Color.red );
    
    // draw grid (skip top and bottom rows)
    for ( int i = 1; i < ((Width / GridSize)-1); i++) {
		for ( int j = 1; j < ((Height / GridSize) - 1); j++) {
			//System.out.println("Drawing: " + i * GridSize + " " + j * GridSize);
			Rectangle2D rect = new Rectangle2D.Double( i * GridSize, j * GridSize, GridSize, GridSize);
			g2d.draw( rect );
		}
    }
    
    // draw display text
    g2d.setColor( Color.black );
    g2d.drawString("Time Step: " + myData.TimeStep, GridSize, (int)Math.floor(0.5 * GridSize));
    g2d.drawString("Remaining: " + (myData.nsteps - myData.TimeStep), 3 * GridSize, (int)Math.floor(0.5 * GridSize));
  }
  
  public static Image makeColorTransparent(final BufferedImage im, final Color color)
   {
      final ImageFilter filter = new RGBImageFilter()
      {
         // the color we are looking for (white)... Alpha bits are set to opaque
         public int markerRGB = color.getRGB() | 0xFFFFFFFF;

         public final int filterRGB(final int x, final int y, final int rgb)
         {
            if ((rgb | 0xFF000000) == markerRGB)
            {
               // Mark the alpha bits as zero - transparent
               return 0x00FFFFFF & rgb;
            }
            else
            {
               // nothing to do
               return rgb;
            }
         }
      };

      final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
      return Toolkit.getDefaultToolkit().createImage(ip);
   }

}
