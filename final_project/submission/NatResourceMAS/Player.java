package NatResourceMAS;

import java.io.*;
import java.util.*;

public class Player {
  
  
 // WEIGHTS:
 public double pop_weight = 0.25;
 public double growth_weight = 0.25;
 public double nearby_housing_weight = 0.25;
 public double mineral_weight = 0.25;
 public double nearby_farm_weight = 0.25;
 public double lumber_availability_weight = 0.25;
 public double existing_forest_weight = 0.25;
 public double fox_weight = 0.25;
 public double foxes_nearby_weight = 0.25;
 public double forest_nearby_weight = 0.25;
 public double timber_opportunity_weight = 0.5;
 public double habitat_destruction_weight = 1.0;
 public double habitat_fragmentation_weight = 0.25;
 public double development_cost_weight = 0.25; // cost to redevelop a property with another existing use
 public int housing_nearby_spread = 2;
 public int forest_nearby_spread = 3;
 public int foxes_nearby_spread = 3;
 
 public String type = "";
 public String land_use = "";
 public double funds = 0;
 public double cumulative_payoffs = 0;
 public int ID = 0;
 public int cells_utilized = 0;
 public static Data myData;
 
 public Player ( String player_type, String land, int ident ) {
  type = player_type;
  land_use = land;
  ID = ident;
  funds = 100000;
  myData = Data.getInstance();
 }
 
 // get player's valuation of cell at row/col
 // valuations always in range [0,1]
 public double valuation(int row, int col) {
  double val = 0; 
  switch(type) {
    case "farmer" :
    {
      if(myData.canFarm(row,col)) {
        if(myData.max_possible_population == 0 || myData.population == 0) {
          val = growth_weight;
        } else {
          val = growth_weight + pop_weight * (myData.population / myData.max_possible_population);
        }
        val += nearby_housing_weight * myData.localHousingFactor(row, col, housing_nearby_spread);
        if(myData.developmentCost(row,col,"FARM")) {
          val -= development_cost_weight;
        }
      }
      break;
    }
    case "miner" :
    {
      
      if(!myData.hasForest(row,col)) {
        if(myData.hasMinerals(row,col)) {
          val = mineral_weight * 1;
        }
        if(myData.developmentCost(row,col,"MINING")) {
          val -= development_cost_weight;
        }
      }

     break;
    }
    case "housing" :
    {
      
      if(!myData.hasForest(row,col)) {
        val = growth_weight;
      
        if(myData.max_possible_population > 0) {
            val += pop_weight * (myData.population / myData.max_possible_population);
        }
        //System.out.println("Pop/Max = " + myData.population + " " + myData.max_possible_population);
        
        if(myData.developmentCost(row,col,"HOUSING")) {
          val -= development_cost_weight;
        }
        
        //System.out.println("Housing val = " + val);
      }
      break;
    }
    case "logging" :
    {
     val = 0;
     if(myData.hasForest(row,col)) {
       val += timber_opportunity_weight;
     }
     if(myData.hasFoxes(row,col) && myData.hasForest(row,col)) {
       val -= habitat_destruction_weight;
     }
     val -= habitat_fragmentation_weight * myData.localFoxesFactor(row,col,foxes_nearby_spread);
     //System.out.println("Logging val = " + val);
  
     break;
    }
    case "nature conservancy" :
    {
     val = 0;
     if(myData.hasForest(row,col)) {
      val += existing_forest_weight;
     
      if(myData.hasFoxes(row,col)) {
        val += fox_weight;
      }
      val += foxes_nearby_weight * myData.localFoxesFactor(row,col,foxes_nearby_spread) + forest_nearby_weight * myData.localForestFactor(row,col,forest_nearby_spread);
     }
     break;
    }
    default :
    {
     val = 0;
    }
  }

      // clamp in range [0,1]
      if(val < 0) {
        val = 0;
      }
      
      if(val > 1) {
        val = 1;
      }
      
  return val;
 }
 
 // virtual valuation
 public double virtual_valuation(int row, int col) {
   return 2 * valuation(row,col) - 1;
 }
 
 // payoff to player
 public void payoff(double pay) {
   cumulative_payoffs += pay;
 }
 
}
