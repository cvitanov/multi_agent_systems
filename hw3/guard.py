'''

GUARD MILP Solution for HW3
By Andrew Cvitanovich

NOTE: Only tested with python version 3.7. May not work with older versions!

To Run: python guard.py

Saves results as a file named solution.csv

Overview:

dpc = "def_payoff_cover"
dpu = "def_payoff_uncover"
apc = "att_payoff_cover"
apu = "att_payoff_uncover"
numr = "number of resources"
ntargets = "number of targets"
alpha = "observational uncertainty"
vdef = "defender's utility"
vatt = "attacker's utility"

'''


import cplex, csv
from cplex.exceptions import CplexError
import sys

# powerset

def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

# Read CSV File Data

# Read Param File
with open('param.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	line_count = 0
	for row in csv_reader:
		if line_count == 0:
			ntargets = int(row[0])
			numr = int(row[1])
			alpha = float(row[2])
			line_count += 1
		else:
			print("Too many parameters!")
	
	#print(f'Processed params with ntargets = {ntargets} and numr = {numr}.')

# power set of targets
tlist = []
for i in range(1,ntargets+1):
	tlist.append(i)
#print(tlist)
pset = list(powerset(tlist))
strategy_list = [s for s in pset if len(s) <= numr]
#print(pset)
#print(strategy_list)
#print(len(strategy_list))
nstrategies = len(strategy_list)

#Payoff Data

dpc = [0.] * ntargets
dpu = [0.] * ntargets
apc = [0.] * ntargets
apu = [0.] * ntargets



with open('payoff.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	line_count = 0
	for row in csv_reader:
		if line_count < ntargets:
			dpc[line_count] = float(row[1])
			dpu[line_count] = float(row[2])
			apu[line_count] = float(row[3])
			apc[line_count] = float(row[4]) # switched columns b.c. of error in data
			line_count += 1
		else:
			print("Too many rows in payoff data!")


# M (must be much larger than possible payoffs)
M = max(max(max(dpc),max(dpu)), max(max(apc),max(apu))) * 100.0


# print(numr)
# print(ntargets)
# print(alpha)

my_obj = [1.0]
for i in range(nstrategies):
	my_obj.append(0.0) # for xt
for i in range(nstrategies):
	my_obj.append(0.0) # for xpt
for i in range(ntargets):
	my_obj.append(0.0) # for vt
my_obj.append(0.0) # for vatt

#print("my_obj " + str(len(my_obj)))

my_colnames = ["vdef"]
my_colnames.append("vatt")
my_xs = []
# for i in range(ntargets):
	# my_xs.append("x"+str(i+1))
for strategy in strategy_list:
	colname = "x"
	for target in strategy:
		colname += str(target)
	my_xs.append(colname)
my_colnames = my_colnames + my_xs
my_xps = []
# for  i in range(ntargets):
	# my_xps.append("xp" + str(i+1))
for strategy in strategy_list:
	colname = "xp"
	for target in strategy:
		colname += str(target)
	my_xps.append(colname)
my_colnames = my_colnames + my_xps
my_zt = []
for i in range(ntargets):
	my_zt.append("z"+str(i+1))
my_colnames = my_colnames + my_zt

#print("my_colnames" + str(len(my_colnames)))

my_ub = [cplex.infinity]
my_ub.append(cplex.infinity)
for i in range(nstrategies):
	my_ub.append(1.0)
for i in range(nstrategies):
	my_ub.append(1.0)
for i in range(ntargets):
	my_ub.append(1.0)


my_lb = [-1.0 * cplex.infinity]
my_lb = my_lb + [-1.0 * cplex.infinity]
my_lb = my_lb + [0.] * (2 * nstrategies +  ntargets)



#print("my_ub" + str(len(my_ub)))
#print("my_lb" + str(len(my_lb)))

my_ctype = "C"
my_ctype = my_ctype + "C"
for i in range(nstrategies):
	my_ctype = my_ctype + "C"
for i in range(nstrategies):
	my_ctype = my_ctype + "C"
for i in range(ntargets):
	my_ctype = my_ctype + "I"


#print("my_ctype" + str(len(my_ctype)))

# Constraints
num_constraints = 3 * ntargets + nstrategies + 2
constraint_names = []
constraints = []
for x in range(num_constraints):
	constraint_names.append("con" + str(x))
# print("constraint_names" + str(len(constraint_names)))
#print(constraint_names)

# Target with highest attacker utility according to perceived strategy
# sum of z_t = 1
con0 = [my_zt, [1.] * ntargets]
constraints.append(con0)

# Defender Strategy
# sum of x's <= number of resources
con1 = [my_xs, [1.] * nstrategies]
constraints.append(con1)

# Optimal target according to perceived strategy
# vatt - Uatt(t,xp_t) >= 0
for i in range(ntargets):
	labels = ["vatt"]
	labels = labels + my_xps
	coeffs = [1.]
	for strategy in strategy_list:
		covered = False
		for target in strategy:
			if(target == (i + 1)):
				covered = True
		if(covered):
			coeffs.append(-1.0 * apc[i])
		else:
			coeffs.append(-1.0 * apu[i])
	my_con = [labels,coeffs]
	constraints.append(my_con)
	labels.append("z"+str(i+1))
	coeffs.append(M)
	my_con = [labels,coeffs]
	constraints.append(my_con)

# perceived strategy
# xp_t = alpha * (1/N) + (1-alpha) * x_t
# Simplified: xp_t + (alpha - 1) * x_t = alpha * (1/N)
for i in range(nstrategies):
	xpt = my_xps[i]
	xt = my_xs[i]
	xt_coeff = alpha - 1.0
	my_con = [[xpt,xt],[1., xt_coeff]]
	constraints.append(my_con)

# Defender Utility
for i in range(ntargets):
	labels = ["vdef"]
	labels = labels + my_xps
	coeffs = [1.]
	for strategy in strategy_list:
		covered = False
		for target in strategy:
			if(target == (i + 1)):
				covered = True
		if(covered):
			coeffs.append(-1.0 * dpc[i])
		else:
			coeffs.append(-1.0 * dpu[i])
	labels.append("z"+str(i+1))
	coeffs.append(M)
	my_con = [labels,coeffs]
	constraints.append(my_con)

constraint_cnt = 0
for c in constraints:
	# print(c)
	constraint_cnt += 1

# print("constraint count = " + str(constraint_cnt))

# rhs

# rhs for Target with highest attacker utility (according to perceived strategy)
rhs = [1.]

# rhs for Defender Strategy
rhs.append(numr)

# rhs for Optimal target according to perceived strategy
for i in range(ntargets):
	rhs.append(0.0)
	rhs.append(M)

# rhs for perceived strategy
for i in range(nstrategies):
	rhs.append(alpha * (1.0 / float(ntargets)))

# rhs for Defender Utility
for i in range(ntargets):
	rhs.append(M)

# print("rhs " + str(len(rhs)))

# constraint_senses

constraint_senses = "EL"

for i in range(ntargets):
	constraint_senses += "G"
	constraint_senses += "L"
for i in range(nstrategies):
	constraint_senses += "E"
for i in range(ntargets):
	constraint_senses += "L"

# print("constraint_senses" + str(len(constraint_senses)))

# SOLVE

# Initialize
p = cplex.Cplex()
#print(p.parameters.simplex.tolerances.feasibility.set(1e-9))
p.objective.set_sense(p.objective.sense.maximize) # max problem
#p.parameters.emphasis.mip.set(2)
#print(p.parameters.mip.strategy.probe.get())
p.set_problem_type(cplex.Cplex.problem_type.MILP)
p.variables.add(obj = my_obj, 
				lb = my_lb,
				ub = my_ub,
				types = my_ctype,
				names = my_colnames)

p.linear_constraints.add(lin_expr = constraints,
						 senses = constraint_senses,
						 rhs = rhs,
						 names = constraint_names)

p.solve()
sol = (p.solution.get_values())
print("Solution:")
#print(my_colnames)
#print(sol)
xes = sol[2:(nstrategies+2)]
print(my_xs)
print(xes)
# print(str(len(my_xs)))
# print(str(len(xes)))

total = sum(xes)
# print("Total = " + str(total))
targets = []
target_coverage = []
for i in range(ntargets):
	the_target = i + 1
	sum_p = 0.0
	sum_of_sums = 0.0
	idx = 0
	for strategy in strategy_list:
		has_target = False
		for target in strategy:
			if(target == the_target):
				has_target = True
		if(has_target):
			sum_p += xes[idx]
		idx += 1
	targets.append(the_target)
	coverage = float(sum_p) / float(numr)
	target_coverage.append(coverage)
	print("Coverage for target " + str(the_target) + " = " + str(coverage))

#print(nstrategies)

# save to csv
with open('solution.csv', mode='w') as outfile:
	w = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	index = 0
	for t in targets:
		w.writerow([t, target_coverage[index]])
		index += 1
