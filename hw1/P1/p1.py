'''

Multiple LP Solution for HW1
By Andrew Cvitanovich

NOTE: Only tested with python version 3.7. May not work with older versions!

To Run: python p1.py

Saves results as a file named SSE.csv

Solution relies on Theorem 2 from the paper referenced in lecture:
https://www.cs.cmu.edu/~sandholm/Computing%20commitment%20strategy.ec06.pdf

Solves each LP (for attacking target i) with the following specification.
Then selects the LP solution with the best utility for the defender and,
then calculates coverage probabilities for each target from the mixed
strategy probabilities.

Overview:

dpc = "def_payoff_cover"
dpu = "def_payoff_uncover"
apc = "att_payoff_cover"
apu = "att_payoff_uncover"
numr = "number of resources"
x{s} = "probability of mixed strategy covering target set s"

MAXIMIZE (for attack on target i):
dpc{i} * x{i in s} + dpu{i} * x{i not in s}

SUBJECT TO:
sum(x{s}) {for all s} = 1
for all s, 1 >= x{s} >= 0

And it is better for attacker to attack i versus other targets for this LP:
for all j not equal to i:
	apc{i} * x{i in s} + apu{i} * x{i not in s} >= apc{j} * x{j in s} + apu[j] * x{j not in s}

(EU of attacker is better for attacking this target versus all others)

Finally, maximize over the utilities for the defender of all pure attacker strategies,
and calculate the coverage probabilities for each target from the mixed strategy
probabilities.

Save results in file named SSE.csv as specified in homework instructions.

'''

import cplex, csv

# powerset

def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

# Read CSV File Data

# Read Param File
with open('param.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	line_count = 0
	for row in csv_reader:
		if line_count == 0:
			ntargets = int(row[0])
			numr = int(row[1])
			line_count += 1
		else:
			print("Too many parameters!")
	
	#print(f'Processed params with ntargets = {ntargets} and numr = {numr}.')


# power set of targets
tlist = []
for i in range(1,ntargets+1):
	tlist.append(i)
#print(tlist)
pset = list(powerset(tlist))
strategy_list = [s for s in pset if len(s) <= numr]
#print(pset)
#print(strategy_list)

# Payoff Data

dpc = [0.] * ntargets
dpu = [0.] * ntargets
apc = [0.] * ntargets
apu = [0.] * ntargets

with open('payoff.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	line_count = 0
	for row in csv_reader:
		if line_count < ntargets:
			dpc[line_count] = float(row[1])
			dpu[line_count] = float(row[2])
			apu[line_count] = float(row[3])
			apc[line_count] = float(row[4]) # switched columns b.c. of error in data
			line_count += 1
		else:
			print("Too many rows in payoff data!")

#print(dpc)
#print(dpu)
#print(apc)
#print(apu)
#print(numr)
#print(ntargets)

best_defender_utility = float("-inf")

# loop through all attack strategies

for this_target in range(1,ntargets+1):
	print("\n\n\n********SOLVE LP FOR ATTACK ON TARGET #{}**********".format(this_target))
	
	# Initialize
	p = cplex.Cplex()
	p.set_problem_type(cplex.Cplex.problem_type.LP)
	p.objective.set_sense(p.objective.sense.maximize) # max problem
	
	my_colnames = []
	my_obj = []
	lb = []
	ub = []
	for strategy in strategy_list:
		lb.append(0.)
		ub.append(1.)
		colname = "x"
		covered = False
		for target in strategy:
			if target == this_target:
				covered = True
			colname += str(target)
		my_colnames.append(colname)
		if covered:
			my_obj.append(dpc[this_target-1])
		else:
			my_obj.append(dpu[this_target-1])
	
	#print("this_target:")
	#print(this_target)
	#print("my_colnames:")
	#print(my_colnames)
	#print("my_obj:")
	#print(my_obj)
	#print("lb:")
	#print(lb)
	#print("ub:")
	#print(ub)
	
	nstrategies = len(strategy_list)
	
	p.variables.add(obj = my_obj, 
	                lb = lb,
	                ub = ub,
	                names = my_colnames)
	
	# Constraints
	num_constraints = ntargets
	constraint_names = []
	for x in range(num_constraints):
		constraint_names.append("con" + str(x))
	#print(constraint_names)
	
	
	constraints = []
	
	# Resource Constraint
	# sum of coverage probabilities equals 1
	con0 = [my_colnames, [1.] * nstrategies]
	constraints.append(con0)
	
	# Cannot have coverage greater than 1 for any target
	# for j in range(1, ntargets + 1):
		# labels = []
		# for strategy in strategy_list:
			# append_strategy = False
			# strategy_label = "x"
			# for target in strategy:
				# strategy_label += str(target)
				# if target == j:
					# append_strategy = True
			# if append_strategy:
				# labels.append(strategy_label)
		# constants = [1.] * len(labels)
		# temp = [labels, constants]
		# constraints.append(temp)
	
	#print(constraints)
	
	# Attacker Constraints
	i = this_target
	for j in range(1, ntargets + 1):
		if j == i:
			pass
		else:
	
			labels = my_colnames
			constants = []
			for strategy in strategy_list:
				covers_target = False
				covers_j = False
				for target in strategy:
					if target == i:
						covers_target = True
					if target == j:
						covers_j = True
				attack_i_payoff = apc[i-1] if covers_target else apu[i-1]
				attack_j_payoff = apc[j-1] if covers_j else apu[j-1]
				payoff = attack_i_payoff - attack_j_payoff
				constants.append(payoff)
	
			temp = [labels, constants]
			constraints.append(temp)
			
	#print(constraints)
	#print(len(constraints))
	
	rhs = [1.] + [0.] * (ntargets - 1)
	
	constraint_senses = ["E"]
	# for x in range(ntargets):
		# constraint_senses.append("L")
	for x in range(ntargets - 1):
		constraint_senses.append("G")
	
	#print("Constraints:")
	#print(constraints)
	#print("rhs:")
	#print(rhs)
	#print("Constraint Names:")
	#print(constraint_names)
	#print("Constraint Senses:")
	#print(constraint_senses)
	
	p.linear_constraints.add(lin_expr = constraints,
	                         senses = constraint_senses,
	                         rhs = rhs,
	                         names = constraint_names)
	
	p.solve()
	sol = (p.solution.get_values())
	#print("Solutions:")
	#print(my_colnames)
	#print(sol)
	index = 0
	for col in my_colnames:
		#print("{} = {}".format(col,sol[index]))
		index += 1
	
	# defender utility
	U = 0
	for i in range(ntargets*2):
		U += sol[i] * my_obj[i]
	
	#print(U)
	
	if(U > best_defender_utility):
		best_defender_utility = U
		best_sol = sol
		
print("\n\n\n\nBest Solution:")
index = 0
for col in my_colnames:
	#print("{} = {}".format(col,best_sol[index]))
	index += 1

targets = []
target_coverage = [0.] * ntargets
# add up mixed strategy coverages to get target coverages
for i in range(ntargets):
	targets.append(i+1)
	index = 0
	for strategy in strategy_list:
		for target in strategy:
			if target == (i+1):
				target_coverage[i] += best_sol[index]
		index += 1

index = 0
for t in targets:
	print("T{} = {}".format(t,target_coverage[index]))
	index += 1

print("\n\nBest Defender Utility = {}".format(best_defender_utility))

# save to csv
with open('SSE.csv', mode='w') as outfile:
	w = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	index = 0
	for t in targets:
		w.writerow([t, target_coverage[index]])
		index += 1
	



print("\n\nSaved solution as file named SSE.csv!")
